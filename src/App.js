import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Layout from "./containers/Layout/Layout";
import Register from './containers/Register/Register';
import Login from './containers/Login/Login';
import Posts from './containers/Posts/Posts';
import Post from './containers/Post/Post';
import NewPost from './containers/NewPost/NewPost';

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Posts}/>
                    <Route path="/register" component={Register}/>
                    <Route path="/login" component={Login}/>
                    <Route path="/new-post" component={NewPost}/>
                    <Route path="/posts/:id" component={Post}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
