import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {fetchPosts} from "../../store/actions/postsActions";
import {Panel, Image, Button, Label} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';

import image from '../../assets/images/image.png';

class Artists extends Component {

    componentDidMount() {
        this.props.fetchPosts();
    };

    render() {
        let url = 'http://localhost:8000/uploads/';
        return (
            <Fragment>
                <Label bsStyle="primary" style={{fontSize: '20px'}}>Posts</Label>
                {this.props.posts.map((post, index) => {
                    return (
                        <Panel style={{marginTop: '20px'}} key={index}>
                            <Panel.Heading className="Header">
                                <strong>
                                    Written by: {post.user}
                                </strong>
                            </Panel.Heading>
                            <Image
                                thumbnail
                                style={{width: '100px', marginRight: '50px'}}
                                src={post.image ? url + post.image : image }
                            />
                            <strong style={{marginRight: '30px'}}>
                                {post.title}
                            </strong>
                            <span style={{marginRight: '30px'}}>{post.description}</span>
                            <NavLink to={'/posts/' + post._id}>
                                <Button bsStyle="primary">Show post</Button>
                            </NavLink>
                        </Panel>
                    )
                })}
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        posts: state.posts.posts
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchPosts: () => dispatch(fetchPosts())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Artists);
