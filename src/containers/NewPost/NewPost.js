import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import NewPostForm from "../../components/NewPostForm/NewPostForm";
import {newPost} from "../../store/actions/postsActions";

class NewPost extends Component {
    newPost = post => {
        this.props.newPost(post).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Create new post</PageHeader>
                <NewPostForm
                    user={this.props.user}
                    newPost={this.newPost} />
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        newPost: post => {
            return dispatch(newPost(post))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewPost);