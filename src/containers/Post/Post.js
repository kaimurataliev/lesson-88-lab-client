import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {fetchPostById, addComment} from "../../store/actions/postsActions";
import {Panel, Image, Form, FormGroup, Col, Button, FormControl, ControlLabel, PageHeader} from 'react-bootstrap'
import image from '../../assets/images/image.png';

class Post extends Component {

    state = {
        description: '',
        postId: null
    };

    componentDidMount() {
        const postId = this.props.match.params.id;
        this.props.fetchPostById(postId);
    }


    submitFormHandler = event => {
        event.preventDefault();
        this.props.addComment(this.state).then(() => {
            const postId = this.props.match.params.id;
            this.props.fetchPostById(postId)
        });
    };

    inputChangeHandler = event => {
        this.setState({[event.target.name]: event.target.value, postId: this.props.match.params.id});
    };

    render() {
        return (
            <Fragment>
                {this.props.onePost &&
                <Panel>
                    {this.props.onePost.post.image ?
                        <Image
                            style={{marginRight: '20px', width: '100px'}}
                            thumbnail
                            src={'http://localhost:8000/uploads/' + this.props.onePost.post.image}
                        /> : <Image
                            style={{marginRight: '20px', width: '100px'}}
                            thumbnail
                            src={image}
                        />}
                    <strong style={{marginRight: '20px'}}>
                        {this.props.onePost.post.title}
                    </strong>
                    <span>
                        {this.props.onePost.post.date}
                    </span>

                </Panel>}

                {this.props.onePost &&
                this.props.onePost.comment.map((comment, index) => {
                    return (
                        <Panel key={index}>
                            <strong>
                                {comment.description}
                            </strong>
                        </Panel>
                    )
                })
                }

                <PageHeader>
                    <small>Enter comment for this post</small>
                </PageHeader>

                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormGroup controlId="description">
                        <Col componentClass={ControlLabel} sm={2}>
                            Title
                        </Col>
                        <Col sm={10}>
                            <FormControl
                                type="text"
                                required
                                placeholder="Enter comment"
                                name="description"
                                value={this.state.description}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button bsStyle="primary" type="submit">Add comment</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        onePost: state.posts.onePost
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchPostById: (id) => dispatch(fetchPostById(id)),
        addComment: (comment) => dispatch(addComment(comment))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Post);