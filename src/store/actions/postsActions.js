import axios from '../../axios';

export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_ERROR = 'FETCH_POSTS_ERROR';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const CREATE_NEW_POST = 'CREATE_NEW_POST';
export const ADD_COMMENT = 'ADD_COMMENT';

export const fetchPostsSuccess = (data) => {
    return {type: FETCH_POSTS_SUCCESS, data}
};

export const fetchPostSuccess = (data) => {
    return {type: FETCH_POST_SUCCESS, data}
};

export const fetchPostsError = (error) => {
    return {type: FETCH_POSTS_ERROR, error};
};

export const fetchPostById = (id) => {
    return dispatch => {
        return axios.get(`/posts/${id}`)
            .then(response => {
                dispatch(fetchPostSuccess(response.data))
            })
    }
};

export const fetchPosts = () => {
    return (dispatch) => {
        return axios.get('/posts')
            .then(response => {
                dispatch(fetchPostsSuccess(response.data))
            }, error => {
                const errorObj = error.response ? error.response.data : "no internet";
                dispatch(fetchPostsError(errorObj))
            })
    }
};

export const newPost = (post) => {
    return dispatch => {
        return axios.post('/posts', post);
    }
};

export const addComment = (comment) => {
    return () => {
        return axios.post('/comments', comment);
    }
};