import {FETCH_POSTS_SUCCESS, FETCH_POSTS_ERROR, FETCH_POST_SUCCESS} from '../actions/postsActions';

const initialState = {
    posts: [],
    onePost: null,
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POSTS_SUCCESS:
            return {...state, posts: action.data};

        case FETCH_POSTS_ERROR:
            return {...state, error: action.error};

        case FETCH_POST_SUCCESS:
            return {...state, onePost: action.data};

        default:
            return state;
    }
};

export default reducer;
