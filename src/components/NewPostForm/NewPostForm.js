import React, {Component} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";

class NewPostForm extends Component {
    state = {
        title: '',
        description: '',
        image: '',
        user: null
    };

    submitFormHandler = event => {
        event.preventDefault();

        const postData = new FormData();
        Object.keys(this.state).forEach(key => {
            postData.append(key, this.state[key]);
        });
        this.props.newPost(postData);
    };

    inputChangeHandler = event => {
        this.setState({[event.target.name]: event.target.value, user: this.props.user.username});
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            this.props.user ? <Form horizontal onSubmit={this.submitFormHandler}>
                <FormGroup controlId="title">
                    <Col componentClass={ControlLabel} sm={2}>
                        Title
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="text"
                            required
                            placeholder="Enter title"
                            name="title"
                            value={this.state.title}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="description">
                    <Col componentClass={ControlLabel} sm={2}>
                        Description
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            componentClass="textarea"
                            placeholder="Enter description"
                            name="description"
                            value={this.state.description}
                            onChange={this.inputChangeHandler}
                            required
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="image">
                    <Col componentClass={ControlLabel} sm={2}>
                        Image
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            required
                            type="file"
                            name="image"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form> : null
        );
    }
}

export default NewPostForm;
